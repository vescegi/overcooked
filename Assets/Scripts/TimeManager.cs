using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    public delegate void RoundsUpdate(int currentRound, int maxRound);
    public static event RoundsUpdate OnRoundsUpdated;

    private const float morningRotation = 10f;
    private const float eveningRotation = 190f;

    private static TimeManager instance;
    public static TimeManager Instance { get =>  instance; } 

    [SerializeField] private Transform sun; // 10 => morning, 190 => night
    [SerializeField] private int roundsToNight;

    private int currentRound;

    private Quaternion baseRotation;

    private void Awake()
    {
        instance = this;
        currentRound = 0;

        baseRotation = sun.rotation;
        InvokeRoundsEvent();
        //sun.rotation = baseRotation;
    }

    private void UpdateSun()
    {
        float increment = (eveningRotation - morningRotation) / (float)roundsToNight;

        sun.Rotate(increment, 0, 0);
    }

    public bool IsDay()
    {
        return currentRound < roundsToNight;
    }

    public void Sleep()
    {
        currentRound = 0;
        sun.rotation = baseRotation;
        InvokeRoundsEvent();
    }

    public void NextRound()
    {
        currentRound++;
        UpdateSun();
        InvokeRoundsEvent();
    }

    private void InvokeRoundsEvent()
    {
        OnRoundsUpdated?.Invoke(currentRound, roundsToNight);
    }
}
