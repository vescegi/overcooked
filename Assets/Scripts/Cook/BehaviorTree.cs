using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BehaviorTree
{
    public enum Status { Success, Running, Failure };

    public abstract class Node
    {

        public Status status;

        public List<Node> children = new List<Node>();
        public int currentChild = 0;
        public string name;

        public Node() { }

        public void AddChild(Node child)
        {
            children.Add(child);
        }

        public abstract Status Process();
    }

    public delegate Status Tick();
    public class Leaf : Node
    {
        public Tick ProcessMethod;

        public Leaf() { }

        public Leaf(string n, Tick pm)
        {
            name = n;
            ProcessMethod = pm;
        }

        public override Status Process()
        {
            Debug.Log(name);

            if (ProcessMethod != null)
                return ProcessMethod();

            Debug.Log("No method");
            return Status.Failure;
        }
    }

    public class Selector : Node
    {
        public Selector(string n)
        {
            name = n;
        }

        public override Status Process()
        {

            Status childStatus = children[currentChild].Process();

            if (childStatus == Status.Running) return Status.Running;
            if (childStatus == Status.Success)
            {
                currentChild = 0;
                return Status.Success;
            }

            currentChild++;
            if (currentChild < children.Count) return Status.Running;

            currentChild = 0;
            return Status.Failure;
        }
    }

    public class Sequence : Node
    {
        public Sequence(string n)
        {
            name = n;
        }

        public override Status Process()
        {
            Status childStatus = children[currentChild].Process();
            if (childStatus == Status.Running) return Status.Running;
            if (childStatus == Status.Failure) return childStatus;

            currentChild++;
            if (currentChild >= children.Count)
            {
                currentChild = 0;
                return Status.Success;
            }

            return Status.Running;
        }
    }

    public class Root : Node
    {
        public Root()
        {
            name = "Root";
        }

        public Root(string n)
        {
            name = n;
        }

        public void PrintTree()
        {
            string treePrintout = "";
            Stack<NodeLevel> nodeStack = new Stack<NodeLevel>();
            Node currentNode = this;
            nodeStack.Push(new NodeLevel { level = 0, node = currentNode });

            while (nodeStack.Count != 0)
            {
                NodeLevel nextNode = nodeStack.Pop();
                treePrintout += new string('-', nextNode.level) + nextNode.node.name + "\n";
                for (int i = nextNode.node.children.Count - 1; i >= 0; i--)
                {
                    nodeStack.Push(new NodeLevel { level = nextNode.level + 1, node = nextNode.node.children[i] });
                }
            }

            Debug.Log(treePrintout);
        }

        public override Status Process()
        {
            return children[currentChild].Process();
        }
    }


    struct NodeLevel
    {
        public int level;
        public Node node;
    }
}
