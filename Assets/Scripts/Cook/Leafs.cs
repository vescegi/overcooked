using UnityEngine;
using UnityEngine.AI;

public class GoToLocation : BehaviorTree.Leaf
{
    private Vector3 position;
    private NavMeshAgent agent;
    public GoToLocation(string locationName, Vector3 position, NavMeshAgent agent)
    {
        this.position = position;
        this.agent = agent;

        base.name = "Go To " + locationName;
        base.ProcessMethod = GoTo;
    }

    private BehaviorTree.Status GoTo()
    {
        agent.SetDestination(position);

        if (ReachedDestination())
        {
            return BehaviorTree.Status.Success;
        }

        return BehaviorTree.Status.Running;
    }

    private bool ReachedDestination()
    {
        if (!agent.pathPending)
        {
            if (agent.remainingDistance <= agent.stoppingDistance)
            {
                if (!agent.hasPath || agent.velocity.sqrMagnitude == 0f)
                {
                    return true;
                }
            }
        }

        return false;
    }
}