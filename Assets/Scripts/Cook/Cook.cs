using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Cook : MonoBehaviour
{
    public delegate void InventoryChange(Ingredients inventory);
    public delegate void RecipeChange(Recipe recipe);

    public static event InventoryChange OnInventoryChanged;
    public static event RecipeChange OnRecipeChanged;

    [SerializeField] private int maxIngredients;

    [Header("Locations")]
    [SerializeField] private Transform bed;
    [SerializeField] private Transform bench;
    [SerializeField] private Transform appleSupplies;
    [SerializeField] private Transform baconSupplies;
    [SerializeField] private Transform cheeseSupplies;
    [SerializeField] private Transform kitchenTable;

    private Ingredients inventory;
    private Ingredients Inventory
    {
        get => inventory; 
        
        set
        {
            inventory = value;
            OnInventoryChanged?.Invoke(inventory);
        }
    }
    private BehaviorTree.Root ai;


    private NavMeshAgent agent;

    private Recipe currentOrder;

    private void Awake()
    {
        Inventory = new Ingredients(maxIngredients, maxIngredients, maxIngredients);
        agent = GetComponent<NavMeshAgent>();

        ai = new();

        BehaviorTree.Sequence flow = new("Flow");
        ai.AddChild(flow);

        Night(flow);
        TakeOrder(flow);
        CheckSupplies(flow, Ingredients.IngredientsName.Apple);
        CheckSupplies(flow, Ingredients.IngredientsName.Cheese);
        CheckSupplies(flow, Ingredients.IngredientsName.Bacon);
        MakeFood(flow);
        ServeFood(flow);
    }

    private void Start()
    {
        Debug.Log(name);
    }
    private void Update()
    {
        ai.Process();
    }

    private void Night(BehaviorTree.Sequence flow)
    {
        BehaviorTree.Selector night = new("Night");
        flow.AddChild(night);

        BehaviorTree.Leaf isDay = new("Is Day", () =>
        {
            if (TimeManager.Instance.IsDay())
                return BehaviorTree.Status.Success;
            else
                return BehaviorTree.Status.Failure;
        });
        night.AddChild(isDay);

        BehaviorTree.Sequence sleepSequence = new("Sleep Sequence");
        night.AddChild(sleepSequence);

        GoToLocation goToBed = new GoToLocation("Bed", bed.position, agent);
        sleepSequence.AddChild(goToBed);

        BehaviorTree.Leaf sleep = new("Sleep", () =>
        {
            TimeManager.Instance.Sleep();
            return BehaviorTree.Status.Success;
        });
        sleepSequence.AddChild(sleep);
    }

    private void TakeOrder(BehaviorTree.Sequence flow)
    {
        BehaviorTree.Sequence takeOrderSequence = new("Take Order");
        flow.AddChild(takeOrderSequence);

        GoToLocation goToBench = new GoToLocation("Bench", bench.position, agent);
        takeOrderSequence.AddChild(goToBench);

        BehaviorTree.Leaf takeOrder = new("Take Order", () =>
        {
            currentOrder = RecipeManager.Instance.GetRandomRecipe();
            OnRecipeChanged?.Invoke(currentOrder);
            return BehaviorTree.Status.Success;
        });
        takeOrderSequence.AddChild(takeOrder);
    }

    private void CheckSupplies(BehaviorTree.Sequence flow, Ingredients.IngredientsName name)
    {
        BehaviorTree.Selector checkSupplies = new("Check supplies");
        flow.AddChild(checkSupplies);

        BehaviorTree.Leaf checkIngredient = new("Check ingredient", () =>
        {
            //Debug.Log(inventory + "\n" + currentOrder.Ingredients);
            if (Inventory.CanMake(currentOrder.Ingredients, name))
            {
                return BehaviorTree.Status.Success;
            }

            return BehaviorTree.Status.Failure;
        });

        checkSupplies.AddChild(checkIngredient);

        BehaviorTree.Sequence replenishSupply = new("Replenish Supply");
        checkSupplies.AddChild(replenishSupply);

        Transform supply = name switch
        {
            Ingredients.IngredientsName.Apple => appleSupplies,
            Ingredients.IngredientsName.Bacon => baconSupplies,
            Ingredients.IngredientsName.Cheese => cheeseSupplies,
            _ => appleSupplies
        };

        GoToLocation goToSupplies = new GoToLocation("Supplies", supply.position, agent);
        replenishSupply.AddChild(goToSupplies);

        BehaviorTree.Leaf fillSupply = new("Fill supplies", () =>
        {
            Inventory.Fill(currentOrder.Ingredients, maxIngredients, name);
            OnInventoryChanged?.Invoke(Inventory);
            return BehaviorTree.Status.Success;
        });
        replenishSupply.AddChild(fillSupply);
    }

    private void MakeFood(BehaviorTree.Sequence flow)
    {
        BehaviorTree.Sequence makeFoodSequence = new("Make Food");
        flow.AddChild(makeFoodSequence);

        GoToLocation goToKitchen = new GoToLocation("Kitchen", kitchenTable.position, agent);
        makeFoodSequence.AddChild(goToKitchen);

        BehaviorTree.Leaf prepareFood = new("Prepare food", () =>
        {
            Inventory -= currentOrder.Ingredients;
            return BehaviorTree.Status.Success;
        });
        makeFoodSequence.AddChild(prepareFood);
    }

    private void ServeFood(BehaviorTree.Sequence flow)
    {
        BehaviorTree.Sequence serveFoodSequence = new("Serve Food");
        flow.AddChild(serveFoodSequence);

        GoToLocation goToBench = new GoToLocation("Bench", bench.position, agent);
        serveFoodSequence.AddChild(goToBench);

        BehaviorTree.Leaf serveFood = new("Serve food", () =>
        {
            TimeManager.Instance.NextRound();
            return BehaviorTree.Status.Success;
        });
        serveFoodSequence.AddChild(serveFood);
    }
}
