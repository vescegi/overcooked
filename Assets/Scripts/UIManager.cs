using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    private static UIManager instance;
    public static UIManager Instance { get => instance; private set => instance = value; }

    [SerializeField] TextMeshProUGUI inventoryText;
    [SerializeField] TextMeshProUGUI recipeNameText;
    [SerializeField] TextMeshProUGUI recipeIngredientsText;
    [SerializeField] TextMeshProUGUI roundsText;

    private void Awake()
    {
        instance = this;
    }

    public void Quit()
    {
        Application.Quit();
    }

    private void ChangeIngredientsText(Ingredients ingredients, TextMeshProUGUI text)
    {
        text.text =
                    ingredients.apple + "\n" +
                    ingredients.bacon + "\n" +
                    ingredients.cheese + "\n";
    }

    public void ChangeInventory(Ingredients ingredients)
    {
        ChangeIngredientsText(ingredients, inventoryText);
    }

    public void ChangeRecipe(Recipe recipe)
    {
        recipeNameText.text = recipe.name;

        ChangeIngredientsText(recipe.Ingredients, recipeIngredientsText);
    }
    
    public void ChangeRounds(int currentRound, int maxRound)
    {
        roundsText.text = "Round: " + currentRound + "/" + maxRound;
    }

    private void OnEnable()
    {
        Cook.OnInventoryChanged += ChangeInventory;
        Cook.OnRecipeChanged += ChangeRecipe;
        TimeManager.OnRoundsUpdated += ChangeRounds;
    }

    private void OnDisable()
    {
        Cook.OnInventoryChanged -= ChangeInventory;
        Cook.OnRecipeChanged -= ChangeRecipe;
        TimeManager.OnRoundsUpdated -= ChangeRounds;

    }
}
