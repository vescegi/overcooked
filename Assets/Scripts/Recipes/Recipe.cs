using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Recipe", menuName = "ScriptableObjects/Recipe")]
public class Recipe : ScriptableObject
{
    [SerializeField] string description;
    [SerializeField] Ingredients ingredients;

    public string Description { get => description; }
    public Ingredients Ingredients { get => ingredients; }
}
