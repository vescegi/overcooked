using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecipeManager : MonoBehaviour
{
    [SerializeField] Recipe[] recipes;

    private static RecipeManager instance;
    public static RecipeManager Instance
    {
        get => instance;
        set => instance = value;
    }

    private void Awake()
    {
        instance = this;
    }

    public Recipe GetRandomRecipe()
    {
        return recipes[Random.Range(0, recipes.Length)];
    }
}
