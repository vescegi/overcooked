
using System.Collections.Generic;

[System.Serializable]
public class Ingredients
{
    public enum IngredientsName
    {
        Apple,
        Bacon,
        Cheese
    }

    public int apple;
    public int bacon;
    public int cheese;

    public Ingredients(int apple, int bacon, int cheese)
    {
        this.apple = apple;
        this.bacon = bacon;
        this.cheese = cheese;
    }


    public static Ingredients operator +(Ingredients a, Ingredients b)
    {
        Ingredients result = new Ingredients(a.apple + b.apple, a.bacon + b.bacon, a.cheese + b.cheese);

        return result;
    }

    public static Ingredients operator -(Ingredients a, Ingredients b)
    {
        Ingredients result = new Ingredients(a.apple - b.apple, a.bacon - b.bacon, a.cheese - b.cheese);

        return result;
    }

    public override string ToString()
    {
        return "Apples: " + apple + " Bacon: " + bacon + " Cheese: " + cheese;
    }

    public bool CanMake(Ingredients recipe, IngredientsName name)
    {
        int myIngredient, otherIngredient;

        switch (name)
        {
            case IngredientsName.Apple:
                myIngredient = apple;
                otherIngredient = recipe.apple;
                break;

            case IngredientsName.Bacon:
                myIngredient = bacon;
                otherIngredient = recipe.bacon;
                break;

            case IngredientsName.Cheese:
                myIngredient = cheese;
                otherIngredient = recipe.cheese;
                break;

            default:
                return false;
        }

        return myIngredient >= otherIngredient;
    }

    public void Fill(Ingredients recipe, int maxFill, IngredientsName name)
    {
        switch (name)
        {
            case IngredientsName.Apple:
                apple = maxFill;
                break;

            case IngredientsName.Bacon:
                bacon = maxFill;
                break;

            case IngredientsName.Cheese:
                cheese = maxFill;
                break;
        }
    }
}
